defmodule Exneuland.SocketHandler do
  @behaviour :cowboy_websocket
  alias Workadventure.Messages

  defp parse_int(x) when is_bitstring(x), do: Integer.parse(x)
  defp parse_int(_), do: :error

  def init(request, _state) do
    args =
      request.qs
      |> String.replace("characterLayers", "characterLayers[]")
      |> String.replace("characterLayer=", "characterLayers[]=")
      |> Plug.Conn.Query.decode()
      |> Enum.map(fn {k, v} ->
        case parse_int(v) do
          :error -> {k, v}
          {x, _} -> {k, x}
        end
      end)
      |> Map.new()

    viewport = %Messages.ViewportMessage{
      left: args["left"],
      top: args["top"],
      right: args["right"],
      bottom: args["bottom"]
    }

    position = %Messages.PositionMessage{
      x: args["x"],
      y: args["y"],
      direction: :DOWN,
      moving: false
    }

    charLayers =
      args["characterLayers"]
      |> Enum.map(&%Messages.CharacterLayerMessage{name: &1})

    state = %{
      room: args["roomId"],
      name: args["name"],
      token: args["token"],
      character: charLayers,
      companion: %Messages.CompanionMessage{name: Map.get(args, "companion", "")},
      position: position,
      viewport: viewport,
      silent: true,
      uid: Enum.random(0..2_147_483_647),
      queue: [],
      peers: [],
      group: nil
    }

    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    Registry.register(Exneuland.Rooms, state.room, {})
    Registry.register(Exneuland.Players, {state.room, state.uid}, {})

    reply =
      %Messages.RoomJoinedMessage{currentUserId: state.uid}
      |> (fn m -> %Messages.ServerToClientMessage{message: {:roomJoinedMessage, m}} end).()

    Exneuland.Rooms.broadcast({:hello, Messages.user_joined_from_state(state)}, state.room)
    send_proto(reply, state)
  end

  def terminate(_reason, _preq, state) do
    leave = {:userLeftMessage, %Messages.UserLeftMessage{userId: state.uid}}
    Exneuland.Rooms.broadcast(leave, state.room)
  end

  def websocket_handle({:binary, message}, state) do
    with {:ok, msg} <- Messages.ClientToServerMessage.decode(message) do
      Exneuland.MessageHandler.handle(msg.message, state)
    else
      err ->
        IO.puts("couldn't decode incoming msg: #{inspect(err)}")
        {:ok, state}
    end
  end

  def websocket_handle(msg, state) do
    IO.puts("unexpected message! #{inspect(msg)}")
    {:ok, state}
  end

  def websocket_info({:send, msg}, state) do
    {:reply, msg, state}
  end

  def websocket_info({:proto, msg}, state), do: send_proto(msg, state)

  def websocket_info({:queue, msg}, state), do: queue(msg, state)

  def websocket_info(:flush, state), do: flush(state)

  def websocket_info(msg, state) do
    Exneuland.InfoHandler.handle(msg, state)
  end

  def send_proto(msg, state) do
    with {:ok, binmsg} <- Protox.Encode.encode(msg) do
      {:reply, {:binary, binmsg}, state}
    else
      err ->
        IO.puts("couldn't encode outgoing msg: #{inspect(err)}, #{inspect(msg)}")
        {:ok, state}
    end
  end

  def queue(msgs, state) when is_list(msgs) do
    if state.queue == [] do
      Process.send_after(self(), :flush, 200)
    end

    {:ok, %{state | queue: msgs ++ state.queue}}
  end

  def queue(msg, state) do
    queue([msg], state)
  end

  defp flush(state) do
    batchPayload =
      state.queue
      |> Enum.map(&%Messages.SubMessage{message: &1})
      |> Enum.reverse()

    batchMsg = %Messages.BatchMessage{payload: batchPayload}
    msg = %Messages.ServerToClientMessage{message: {:batchMessage, batchMsg}}
    websocket_info({:proto, msg}, %{state | queue: []})
  end
end
