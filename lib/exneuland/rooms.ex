defmodule Exneuland.Rooms do
  def broadcast(msg, room), do: dispatch(Exneuland.Rooms, room, msg)

  def send_to(msg, {_room, _uid} = key), do: dispatch(Exneuland.Players, key, msg)

  def cast_group(msg, {_room, _gid} = key), do: dispatch(Exneuland.Groups, key, msg)

  defp dispatch(registry, key, msg) do
    Registry.dispatch(registry, key, fn entries ->
      for {pid, _} <- entries do
        if pid != self() do
          send(pid, msg)
        end
      end
    end)
  end
end
