defmodule Exneuland.Group do
  alias Workadventure.Messages

  def start(room, gid, participants) do
    # IO.puts "Group #{gid} in #{room}: #{inspect participants}"
    {:ok, spawn(Exneuland.Group, :init, [room, gid, participants])}
  end

  def init(room, gid, participants) do
    [{uA, nA, pA}, {uB, _nB, _pB}] = participants

    state = %{
      gid: gid,
      room: room,
      center: %{x: 0, y: 0},
      participants: Map.new([{uA, {nA, pA}}])
    }

    Registry.register(Exneuland.Groups, {room, gid}, {})
    Registry.register(Exneuland.Rooms, room, {})

    request_join(gid, room, uB)
    # Recalculate memberships after a second, in case init didn't work
    Process.send_after(self(), :calc, 1000)

    loop(state)
  end

  defp loop(state) do
    receive do
      x ->
        case handle(x, state) do
          {:ok, state} -> loop(state)
          {:quit, state} -> quit(state)
          x -> exit({:error, x})
        end
    end
  end

  defp handle({:userMovedMessage, msg}, state) do
    dist = Messages.distance(msg.position, state.center)

    if msg.userId in Map.keys(state.participants) do
      if dist > 60 do
        leave(msg.userId, state)
      else
        newpart =
          Map.update!(state.participants, msg.userId, fn {name, _pos} -> {name, msg.position} end)

        calc(%{state | participants: newpart})
      end
    else
      if dist < 45 do
        request_join(state.gid, state.room, msg.userId)
      end

      {:ok, state}
    end
  end

  defp handle({:join, {uid, name, position}}, state) do
    for {peer, {pname, _ppos}} <- state.participants do
      connect({peer, pname}, uid, state.room)
      connect({uid, name}, peer, state.room)
    end

    calc(%{state | participants: Map.put(state.participants, uid, {name, position})})
  end

  defp handle({:hello, msg}, state) do
    reply = {
      :groupUpdateMessage,
      %Messages.GroupUpdateMessage{
        groupId: state.gid,
        position: %Messages.PointMessage{x: state.center.x, y: state.center.y},
        groupSize: 60
      }
    }

    Exneuland.Rooms.send_to(reply, {state.room, msg.userId})
    {:ok, state}
  end

  defp handle({:userLeftMessage, msg}, state), do: leave(msg.userId, state)
  defp handle(:calc, state), do: calc(state)
  defp handle(_, state), do: state

  defp calc(state) do
    if length(Map.keys(state.participants)) > 1 do
      newcenter = recenter(state)
      {:ok, %{state | center: newcenter}}
    else
      {:quit, state}
    end
  end

  defp leave(uid, state) do
    newpart = Map.delete(state.participants, uid)

    for {peer, _data} <- newpart do
      disconnect(peer, uid, state.room)
      disconnect(uid, peer, state.room)
    end

    calc(%{state | participants: newpart})
  end

  defp quit(state) do
    Exneuland.Rooms.broadcast(
      {:groupDeleteMessage, %Messages.GroupDeleteMessage{groupId: state.gid}},
      state.room
    )

    exit(:normal)
  end

  defp connect({target, name}, source, room) do
    Exneuland.Rooms.send_to(
      {:webRtcStartMessage,
       %Messages.WebRtcStartMessage{userId: target, name: name, initiator: target > source}},
      {room, source}
    )
  end

  defp disconnect(p1, p2, room) do
    Exneuland.Rooms.send_to(
      {:webRtcDisconnectMessage, %Messages.WebRtcDisconnectMessage{userId: p1}},
      {room, p2}
    )
  end

  def request_join(gid, uid, room) do
    Exneuland.Rooms.send_to({:joinGroup, gid}, {uid, room})
  end

  defp recenter(state) do
    {x, y} = Messages.middle(Enum.map(state.participants, fn {_uid, {_name, pos}} -> pos end))

    Exneuland.Rooms.broadcast(
      {
        :groupUpdateMessage,
        %Messages.GroupUpdateMessage{
          groupId: state.gid,
          position: %Messages.PointMessage{x: x, y: y},
          groupSize: 60
        }
      },
      state.room
    )

    %{x: x, y: y}
  end
end
