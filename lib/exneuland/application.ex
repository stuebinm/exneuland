defmodule Exneuland.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {
        Plug.Cowboy,
        scheme: :http,
        plug: Exneuland.Router,
        options: [
          port: 4000,
          dispatch: dispatch()
        ]
      },
      {Registry, keys: :duplicate, name: Exneuland.Rooms},
      {Registry, keys: :unique, name: Exneuland.Groups},
      {Registry, keys: :unique, name: Exneuland.Players}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Exneuland.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {
        :_,
        [
          {"/pusher/room", Exneuland.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {Exneuland.Router, []}}
        ]
      }
    ]
  end
end
