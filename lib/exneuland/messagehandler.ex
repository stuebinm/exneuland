defmodule Exneuland.MessageHandler do
  alias Workadventure.Messages

  def handle({:silentMessage, message}, state) do
    {:ok, %{state | silent: message.silent}}
  end

  def handle({:userMovesMessage, msg}, state) do
    update = {
      :userMovedMessage,
      %Messages.UserMovedMessage{
        userId: state.uid,
        position: msg.position
      }
    }

    Exneuland.Rooms.broadcast(update, state.room)

    {:ok, %{state | position: msg.position, viewport: msg.viewport}}
  end

  def handle({:viewportMessage, msg}, state) do
    {:ok, %{state | viewport: msg}}
  end

  def handle(nil, state), do: {:ok, state}

  def handle({:webRtcSignalToServerMessage, msg}, state) do
    if msg.receiverId in state.peers do
      inner = %Messages.WebRtcSignalToClientMessage{
        userId: state.uid,
        signal: msg.signal
      }

      {:proto, %Messages.ServerToClientMessage{message: {:webRtcSignalToClientMessage, inner}}}
      |> Exneuland.Rooms.send_to({state.room, msg.receiverId})
    else
      IO.puts("Stray WebRTC Signal from #{state.name}@#{state.uid}")
    end

    {:ok, state}
  end

  # Handle decoded WS ClientToServer messages
  def handle(message, state) do
    # this is just a fallthrough in case we haven't implemented the message type yet
    IO.inspect(message)
    {:ok, state}
  end
end
