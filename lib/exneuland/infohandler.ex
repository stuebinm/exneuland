defmodule Exneuland.InfoHandler do
  alias Workadventure.Messages

  def handle({:hello, msg}, state) do
    reply = {:userJoinedMessage, Messages.user_joined_from_state(state)}
    Exneuland.Rooms.send_to(reply, {state.room, msg.userId})

    Exneuland.SocketHandler.queue({:userJoinedMessage, msg}, state)
  end

  def handle({:userMovedMessage, msg} = cmsg, state) do
    dist = Messages.distance(msg.position, state.position)
    moving = msg.position.moving or state.position.moving

    if dist <= 60 and !state.silent and !moving and state.group == nil do
      {:initGroup, {state.uid, state.name, state.position}}
      |> Exneuland.Rooms.send_to({state.room, msg.userId})
    end

    Exneuland.SocketHandler.queue(cmsg, state)
  end

  def handle({:initGroup, peer}, state) do
    if state.group == nil and !state.silent do
      gid = Enum.random(0..2_147_483_647)
      Exneuland.Group.start(state.room, gid, [{state.uid, state.name, state.position}, peer])
      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  def handle({:joinGroup, gid}, state) do
    if state.group == nil and !state.silent and !state.position.moving do
      Exneuland.Rooms.cast_group(
        {:join, {state.uid, state.name, state.position}},
        {state.room, gid}
      )

      {:ok, %{state | group: gid}}
    else
      {:ok, state}
    end
  end

  def handle({:webRtcStartMessage, msg} = cmsg, state) do
    Exneuland.SocketHandler.send_proto(
      %Messages.ServerToClientMessage{message: cmsg},
      %{state | peers: [msg.userId | state.peers]}
    )
  end

  def handle({:webRtcDisconnectMessage, msg} = cmsg, state) do
    group =
      case length(Enum.uniq(state.peers)) do
        1 -> nil
        _ -> state.group
      end

    Exneuland.SocketHandler.send_proto(
      %Messages.ServerToClientMessage{message: cmsg},
      %{state | peers: Enum.reject(state.peers, &(&1 == msg.userId)), group: group}
    )
  end

  def handle({t, msg}, state) do
    Exneuland.SocketHandler.queue({t, msg}, state)
  end

  def handle(msg, state) do
    IO.puts("unknown info: #{inspect(msg)}")
    {:ok, state}
  end
end
