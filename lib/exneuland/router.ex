defmodule Exneuland.Router do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  use Plug.Debugger

  match _ do
    send_resp(conn, 404, "not found")
  end
end
