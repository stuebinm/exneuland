defmodule Workadventure.Messages do
  @external_resource "./protos/messages.proto"
  use Protox, files: ["./protos/messages.proto"], namespace: Workadventure.Messages
  alias Workadventure.Messages

  def wrap_batch_s2c(msg) do
    %Messages.ServerToClientMessage{
      message: {
        :batchMessage,
        %Messages.BatchMessage{
          payload: [
            %Messages.SubMessage{
              message: msg
            }
          ]
        }
      }
    }
  end

  def user_joined_from_state(state) do
    %Messages.UserJoinedMessage{
      userId: state.uid,
      name: state.name,
      characterLayers: state.character,
      position: state.position,
      companion: state.companion
    }
  end

  def distance(a, b) do
    import :math
    round(sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2)))
  end

  def middle(positions) do
    {xsum, ysum} = List.foldl(positions, {0, 0}, fn pos, {x, y} -> {x + pos.x, y + pos.y} end)
    l = length(positions)
    {round(xsum / l), round(ysum / l)}
  end
end
